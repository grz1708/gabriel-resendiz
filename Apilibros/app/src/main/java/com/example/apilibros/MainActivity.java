package com.example.apilibros;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.apilibros.Interfaces.ProductoApi;
import com.example.apilibros.modelos.Libro;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {
    private TextView datos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        datos=findViewById(R.id.datos);
        Libros();
    }

    private  void Libros()
    {
        Retrofit retrofit= new  Retrofit.Builder()
                .baseUrl("https://www.etnassoft.com/api/v1/get/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ProductoApi productoApi = retrofit.create(ProductoApi.class);
        Call<List<Libro>> call=productoApi.getLibro();
        call.enqueue(new Callback<List<Libro>>() {
            @Override
            public void onResponse(Call<List<Libro>> call, Response<List<Libro>> response) {
                if(!response.isSuccessful())
                {
                    datos.setText("Codigo:"+response.code());
                    return;
                }
                List<Libro> libroList=response.body();
                for( Libro libro: libroList)
                {
                    String content="";
                    content+="ID: "+ libro.getID()+"\n";
                    content+="title: "+ libro.getTitle()+"\n";
                    content+="author: "+ libro.getAuthor()+"\n";
                    content+="content: "+ libro.getContent()+"\n";
                    content+="content_short: "+ libro.getContent_short()+"\n";
                    content+="publisher: "+ libro.getPublisher()+"\n";
                    content+="publisher_date: "+ libro.getPublisher_date()+"\n";
                    content+="pages: "+ libro.getPages()+"\n";
                    content+="language: "+ libro.getLanguage()+"\n";
                    content+="url_details: "+ libro.getUrl_details()+"\n";
                    content+="url_download: "+ libro.getUrl_download()+"\n";
                    content+="cover: "+ libro.getCover()+"\n";
                    content+="thumbnail: "+ libro.getThumbnail()+"\n";
                    content+="num_comment: "+ libro.getNum_comments()+"\n\n";
                    datos.append(content);
                }
            }

            @Override
            public void onFailure(Call<List<Libro>> call, Throwable t) {
            datos.setText(t.getMessage());
            }
        });
    }
}