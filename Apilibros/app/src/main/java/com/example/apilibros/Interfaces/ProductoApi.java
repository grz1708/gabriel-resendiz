package com.example.apilibros.Interfaces;

import com.example.apilibros.modelos.Libro;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface ProductoApi {
    @GET("?category=libros")
    Call<List<Libro>> getLibro();

}
