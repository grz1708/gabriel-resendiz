package com.example.juegopractica;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button acepta =(Button) findViewById(R.id.Aceptar);//crea una varia de tipo boton con la vista
        acepta.setOnClickListener(new View.OnClickListener() {//crea el evento para el boton
            @Override
            public void onClick(View v) {
                RadioGroup ra =(RadioGroup) findViewById(R.id.radioGroup4);// crea una  variable  de tipo radio Group

                validar(); //se manda a llamar la funcion
                ra.clearCheck();//limpia los radio button
            }
        });
    }
    public void  validar()//se crea la funcion validar
    {
        RadioButton m=(RadioButton) findViewById(R.id.radioButton4);//se crea un elemento de tipo radiobutton
        RadioButton h=(RadioButton) findViewById(R.id.radioButton3);//se crea un elemento de tipo radiobutton
        if(h.isChecked()) {//verifica si los radio buton estan validado
            String nombre, apellido, edad;//se crean  3 variables  de tipo string
            String[] nombres = {"Rosa", "Maria", "Elena", "Bere", "Karieme"};// se crea un aregrego de tipo string
            String[] hijos = {"1", "2", "3", "4", "5"};// se crea un aregrego de tipo string
            Random nom = new Random();// se crea una funcion de tipo random
            int numerochica = nom.nextInt(nombres.length);//se genera numeros ramdom
            TextView resul = (TextView) findViewById(R.id.Resul);//se genera una bariable  de tipo TexView
            EditText nomc = (EditText) findViewById(R.id.editApellido);//se gener auna variable de tipo Edit tex
            nombre = nomc.getText().toString();//se  igualan las variables
            EditText ape = (EditText) findViewById(R.id.editNombre);//se genera una variable de tipo Edit tex
            apellido = ape.getText().toString();//se igualan las variables
            EditText ed = (EditText) findViewById(R.id.edad);//se genera una variables de tipo edid Tex
            edad = ed.getText().toString();//se igualan las variables
            nomc.setText("");//se limpian las EditTex
            ape.setText("");//se limpian las EditTex
            ed.setText("");//se limpian las EditTex
            resul.setText("Tu nombre es " + nombre + " " + apellido + " Tu edad es " + edad + " años te casaras con " + nombres[numerochica] + " y tendras " + hijos[numerochica] + " hijos");//Se manda el mensaje completo

        }
        if(m.isChecked()) {//verifica si los radio buton estan validado
            String nombre, apellido, edad;//se crean  3 variables  de tipo string
            String[] nombres = {"Rodrigo", "Mario", "Ernesto", "Beto", "Gabriel"};// se crea un aregrego de tipo string
            String[] hijos = {"1", "2", "3", "4", "5"};// se crea un aregrego de tipo string
            Random nom = new Random();// se crea una funcion de tipo random
            int numerochica = nom.nextInt(nombres.length);//se genera numeros ramdom
            TextView resul = (TextView) findViewById(R.id.Resul);//se genera una bariable  de tipo TexView
            EditText nomc = (EditText) findViewById(R.id.editApellido);//se gener auna variable de tipo Edit tex
            nombre = nomc.getText().toString();//se  igualan las variables
            EditText ape = (EditText) findViewById(R.id.editNombre);//se genera una variable de tipo Edit tex
            apellido = ape.getText().toString();//se igualan las variables
            EditText ed = (EditText) findViewById(R.id.edad);//se genera una variables de tipo edid Tex
            edad = ed.getText().toString();//se igualan las variables
            nomc.setText("");//se limpian las EditTex
            ape.setText("");//se limpian las EditTex
            ed.setText("");//se limpian las EditTex
            resul.setText("Tu nombre es " + nombre + " " + apellido + " Tu edad es " + edad + " años te casaras con " + nombres[numerochica] + " y tendras " + hijos[numerochica] + " hijos");//Se manda el mensaje completo
        }

    }


}