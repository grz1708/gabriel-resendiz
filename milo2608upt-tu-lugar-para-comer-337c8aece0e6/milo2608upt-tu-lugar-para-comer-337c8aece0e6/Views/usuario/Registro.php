<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="../../../proyect/css/styleRegistro.css">
    <title>Login - Tu Lugar Para Comer</title>
</head>
<body>
    <section class="contact-box">
        <div class="row no-gutters bg-dark">
            <div class="col-xl-5 col-lg-12 register-bg">

            </div>
            <div class="col-xl-7 col-lg-12 d-flex">
                <div class="container align-self-center p-6">
                    <h1 class="font-weight-bold">Crea tu cuenta gratis</h1>
                    <div class="form-group">
                        <button class="btn btn-outline-dark d-inline-block text-light mr-2 mb-3"><ion-icon name="logo-google" class="lead align-middle mr-2"></ion-icon> Google</button>
                        <button class="btn btn-outline-dark d-inline-block text-light mb-3"><ion-icon name="logo-facebook" class="lead align-middle mr-2"></ion-icon>Facebook</button>
                    </div>
                    <p class="text-muted mb-5">Ingresa la siguiente informacion para registrarte.</p>
                    <form action="?controller=usuarios&action=insertar" method="POST">
                        <?php
                        if(isset($_SESSION["flash"])) {
                            echo "<p class='msj alert alert-primary'>".$_SESSION["flash"]."</p>";
                        }
                        unset($_SESSION["flash"]);
                        ?>
                        <div class="form-row mb-2">
                            <div class="form-group col-md-6">
                                <label class="font-weight-bold">Nombre<span class="text-danger">*</span></label>
                                <input required type="text" class="form-control" name="nombre" id="nombre" placeholder="Tu Nombre">
                            </div>
                            <div class="form-group col-md-6">
                                <label class="font-weight-bold">Apellido<span class="text-danger">*</span></label>
                                <input required type="text" class="form-control" name="apellido" id="apellido" placeholder="Tu Apellido">
                            </div>
                        </div>
                        <div class="form-group mb-3">
                            <label class="font-weight-bold">Correo electronico<span class="text-danger">*</span></label>
                            <input required type="email" name="correo" id="correo" class="form-control" placeholder="Ingresa tu correo electronico">
                        </div>
                        <div class="form-group mb-3">
                            <label class="font-weight-bold">Contraseña<span class="text-danger">*</span></label>
                            <input required type="password"name="contraseña" id="contraseña" class="form-control" placeholder="Ingresa una contraseña">
                        </div>
                        <div class="form-group mb-5">
                            <div class="form-check">
                                <input type="checkbox" class="form-check-input" required checked>
                                <label class="form-check-label text-muted">Al seleccionar esta casilla aceptas buestro aviso de privacidad y los terminos y condiciones</label>
                            </div>
                        </div>
                        <input class="btn btn-primary" type="submit" value="Registrar">
                        <a href="?controller=Usuarios&action=login" class="btn btn-light letter">Regresar</a>
                    </form>
                </div>
            </div>
        </div>
    </section>
</body>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <script src="https://unpkg.com/ionicons@5.0.0/dist/ionicons.js"></script>
</html>
