<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Foode - Restaurantes</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="../../../proyect/css/styleCard.css">
</head>
<body>
<div>
    <a href="?controller=restaurantes&action=nuevo" class="btn btn-success" style="margin-top: 40px; margin-left: 100px">AGREGAR RESTAURANTE</a>
    <a href="?controller=usuarios&action=init" class=" btn btn-dark" style="margin-top: 40px; margin-left: 100px"> REGRESAR</a>
</div>
<!-- Swiper -->
<div class="container">

</div>

</body>
<script src="../../../proyect/js/jquery.min.js"></script>
<script src="../../../proyect/js/show.js"></script>
</html>