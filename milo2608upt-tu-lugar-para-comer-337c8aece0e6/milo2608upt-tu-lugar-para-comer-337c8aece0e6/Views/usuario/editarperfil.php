<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="../../../proyect/css/styles.css">
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <title>Edit profile</title>
</head>
<body>
<div class="form-bg">

    <div class="container">
        <div id="add"></div>
        <div class="row">
            <div class="col-md-6 offset-md-3">
                <form action="?controller=usuarios&action=actualizar" method="post" id="forreg" class="form-horizontal">
                    <div class="form-icon"><i class="fa fa-user-circle"></i></div>
                    <h3 class="title">Edit profile</h3>
                    <div class="form-group">
                        <span class="input-icon"><i class="fas fa-hand-point-right"></i></span>
                        <input type="hidden" id="id" name="id" value="<?php echo $_SESSION["id"]?>">
                        <input class="form-control" type="text" name="nombre" id="nombre" value="<?php echo $_SESSION["nombre"]?>">
                    </div>
                    <div class="form-group">
                        <span class="input-icon"><i class="fa fa-user"></i></span>
                        <input class="form-control" type="text" name="apellido" id="apellido" value="<?php echo $_SESSION["apellido"]?>">
                    </div>
                    <div class="form-group">
                        <span class="input-icon"><i class="fas fa-envelope"></i></span>
                        <input class="form-control" type="email" name="correo" id="correo" value="<?php echo $_SESSION["correo"]?>">
                    </div>
                    <div class="form-group">
                        <span class="input-icon"><i class="fa fa-lock"></i></span>
                        <input class="form-control" type="text" name="contraseña" id="contraseña" value="<?php echo $_SESSION["contraseña"]?>">
                    </div>
                    <input TYPE="submit" class="btn logout" id="guardar" value="GUARDAR">
                    <a href="?controller=usuarios&action=init"class="exit"">REGRESAR</a>
                </form>
            </div>
        </div>
    </div>
</div>
</body>
<script src="../js/jquery.min.js"></script>
<script src="https://kit.fontawesome.com/920f985a19.js" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</html>