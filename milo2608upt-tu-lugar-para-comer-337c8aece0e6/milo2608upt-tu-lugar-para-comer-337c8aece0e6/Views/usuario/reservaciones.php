<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="../../css/styles.css">
    <link rel="stylesheet" href="../../css/bootstrap.min.css">
<link rel="icon" href="../../img/alba-logo.png" type="image/jpg" />
    <title>Mis reservaciones</title>
</head>
<style>
    .content-table{
        border-collapse: collapse;
        margin: 25px 0;
        font-size: 0.9rem;
        min-width: 480px;
    }

    .content-table thead tr{
        background-color: #c69500;
        color: #FFFFFF;
        text-align: left;
        font-weight: bold;
    }

    .content-table th,
    .content-table td{
        padding: 12px 15px;
    }

    .content-table tbody tr{
         border-bottom: 1px solid #dddddd;
     }

    .content-table tbody tr:nth-of-type(even){
        background-color: #f3f3f3;
    }

    .content-table tbody tr:last-of-type{
        border-bottom: 2px solid #c69500;
    }

    h3{
        text-align: center;
        margin-top: 100px;
    }
</style>
<body>

        
        <div class="container">
                    <h3 class="title">Mis reservaciones</h3>
                    <table class="table content-table">
                        <thead>
                            <tr>
                            	<th>Nº de resrvacion</th>
                                <th>nombre cliente</th>
                                <th>fecha</th>
                                <th>hora</th>
                                <th>Eliminar</th>
                                <th>Editar</th>
                            </tr>
                        </thead>
                        <tbody id="tabla">

                        </tbody>
                    </table>
            <a href="?controller=usuarios&action=verres" class="btn btn-success">Nueva Reservacion</a>
        </div>

    </div>
</body>
<script src="../../../proyect/js/jquery.min.js"></script>
<script src="../../../proyect/js/tabla.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</html>
