<?php
session_start();
use proyect\Model\Usuario;
include 'Model/Conexion.php';
include 'Model/Usuario.php';
    class usuariosController{

        function __construct() {
           
        }

        function login(){
            require_once "Views/usuario/Login.php";
        }

        function verres(){
            require_once "Views/usuario/show.php";
        }

        function ver(){
            require_once "Views/usuario/ver.php";
        }
        function editarperfil(){
            require_once "Views/usuario/editarperfil.php";
        }

        function site(){
            require_once "Views/usuario/site.php";
        }

        function init(){
            require_once "Views/usuario/init.php";
        }

        function registrar(){
            require_once "Views/usuario/Registro.php";
        }

        function cerrarsesion(){
            session_abort();
            header("Location:?controller=usuarios&action=login");
        }

        function insertar(){
            $usuario=new Usuario();
            $usuario->nombre=$_POST["nombre"];
            $usuario->apellido=$_POST["apellido"];
            $usuario->correo=$_POST["correo"];
            $usuario->contraseña=$_POST["contraseña"];
            if($_POST["nombre"]==""||$_POST["apellido"]==""||$_POST["correo"]==""||$_POST["contraseña"]==""){
                $_SESSION["flash"] = "Campos Vacios";
                header("Location:?controller=usuarios&action=registrar");
            }else{
                $usuario->insert();
                $_SESSION["flash"] = "Usuario Registrado";
                header("Location:?controller=usuarios&action=login");
            }
            
        }

        function datos(){
            $usuario=Usuario::show();
            echo json_encode($usuario);
        }

        function buscarusuario(){
            $usuario=$_POST["usuario"];
            if($usuario!=""){
                $res=Usuario::findByUsuario($usuario);
                if($res["usuario"]!=$usuario){
                    echo "no existe usuario";
                }
                else{
                    echo json_encode($res);
                }
            }else{
                echo "error sin paraetros";
            }
            
            
        }

        function buscar($usuario){
            $id=$_POST["id"];
            if($id!=""){
                $res=Usuario::find($usuario);
                if($res["id"]!=$usuario){
                    echo "no existe usuario";
                
                }else{
                    echo json_encode(["estatus"=>"success","msj"=>$res]);
                }
            }else{
                echo json_encode(["estatus"=>"success","msj"=>"error"]);
            }
        }

        function actualizar(){
            $usuario=new Usuario();
            $usuario->id=$_POST["id"];
            $usuario->nombre=$_POST["nombre"];
            $usuario->apellido=$_POST["apellido"];
            $usuario->correo=$_POST["correo"];
            $usuario->contraseña=$_POST["contraseña"];
            if($_POST["nombre"]==""||$_POST["apellido"]==""||$_POST["correo"]==""||$_POST["contraseña"]==""||$_POST["id"]==""){
                $_SESSION["flash"] = "Ocurrio un error";
                header("Location:?controller=usuarios&action=editarperfil");
            }else{
                $usuario->update();
                $_SESSION["flash"] = "Usuario Actualizado";
                header("Location:?controller=usuarios&action=login");
            }
            
        }


        function eliminar(){
            $id=$_POST["id"];
            $res=Usuario::delete($id);
            $_SESSION["flash"] = "Usuario Eliminado";
            header("Location:?controller=usuarios&action=login");
        }

        function validar(){
            if (isset($_POST)) {
                $usuario = Usuario::findByEmail($_POST["correo"]);
                if ($usuario == NULL) {
                    $_SESSION["flash"] = "Usuario incorrecto";
                    header("Location:?controller=usuarios&action=login");
                } else {
                    if ($_POST["contraseña"]==$usuario->contraseña) {
                        $_SESSION["id"] = $usuario->id;
                        $_SESSION["nombre"] = $usuario->nombre;
                        $_SESSION["apellido"] = $usuario->apellido;
                        $_SESSION["correo"] = $usuario->correo;
                        $_SESSION["contraseña"] = $usuario->contraseña;
                        $_SESSION["usuario"] = $usuario->nombre." ".$usuario->apellido;
                        //echo json_encode($usuario);
                        header("Location:?controller=Usuarios&action=init");
                    } else {
                        $_SESSION["flash"] = "Contraseña incorrecta";
                        header("Location:?controller=Usuarios&action=login");
                    }
                }
            }
            
        }
    }