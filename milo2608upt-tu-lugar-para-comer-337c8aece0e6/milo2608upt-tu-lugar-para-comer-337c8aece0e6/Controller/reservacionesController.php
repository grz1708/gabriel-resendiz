<?php 
session_start();
use proyect\Model\Reservaciones;
include 'Model/Conexion.php';
include 'Model/Reservaciones.php';
    class reservacionesController{

        function __construct() {
           
        }

        function verreservas(){
            require_once "Views/usuario/reservaciones.php";
        }

        function editreservas(){
            require_once "Views/usuario/editarreservaciones.php";
        }

         function insertar(){
            $reservaciones=new Reservaciones();
           	$reservaciones->id_usuario=$_SESSION["id"];
            $reservaciones->id_restaurante=$_SESSION["idres"];
            $reservaciones->nom_cliente=$_SESSION["usuario"];
             $inicio = strtotime($_POST["fecha"]);
            $fecha= date('Y-m-d',$inicio);
            $reservaciones->fecha=$fecha;
            $reservaciones->hora=$_POST["hora"];
            if($_POST["hora"]==""||$_POST["fecha"]==""){
                $_SESSION["flash"] = "Campos Vacios";
                header("Location:?controller=usuarios&action=login");
            }else{
                $reservaciones->insert();
                $_SESSION["flash"] = "Reservacion hecha con exito";
               header("Location:?controller=restaurantes&action=reservar");
                //echo $fecha;
            }
        }
         function buscarnombre(){
            $nom_cliente=$_POST["nom_cliente"];
            if($nom_cliente!=""){
                $res=Reservaciones::findBynombrecliente($nom_cliente);
                if($res["Nom_Cliente"]!=$nom_cliente){
                    echo "no existe usuario";
                
                }else{
                    echo json_encode($res);
                }
            }else{
                echo "error sin paraetros";
            }
        }

        function buscar(){
            $id=$_POST["id"];
            if($id!=""){
                $res=Reservaciones::find($id);
                $_SESSION["idreser"]=$res["Id"];
                $_SESSION["iduser"]=$res["Id_Usuario"];
                $_SESSION["idresta"]=$res["Id_Restaurante"];
                $_SESSION["no"]=$res["Nom_Cliente"];
                $_SESSION["hora"]=$res["Hora"];
                $_SESSION["fecha"]=$res["Fecha"];
                header("Location:?controller=reservaciones&action=editreservas");
            }else{
                echo "ERROR";
            }
        }
         function read(){
            $idusuario=$_SESSION["id"];
            $res=Reservaciones::selectall($idusuario);
            echo json_encode(["estado"=>"true","datos"=>$res]);
        }

         function actualizar()
         {
             $reservaciones = new Reservaciones();
             $reservaciones->id = $_POST["id"];
             $reservaciones->id_usuario = $_SESSION["iduser"];
             $reservaciones->id_restaurante = $_SESSION["idresta"];
             $reservaciones->nom_cliente = $_POST["nombre"];
             $reservaciones->fecha = $_POST["fecha"];
             $reservaciones->hora = $_POST["hora"];

             if ($_POST["hora"] == "" || $_POST["fecha"] == "" || $_POST["nombre"] == "" || $_POST["id"] == "") {
                 echo json_encode(["estatus" => "error", "usuario" => "error"]);
             } else {
                 $reservaciones->update();
                 $_SESSION["flash"] = "Reservacion modificada";
                 header("Location:?controller=reservaciones&action=editreservas");
             }
         }


        function eliminar(){
            $id=$_POST["id"];
            if($_POST["id"]==""){
                echo json_encode(["estatus"=>"error","msj"=>"error"]);
            }else{
                Reservaciones::delete($id);
                header("Location:?controller=reservaciones&action=verreservas");
            }
        }
    }
 ?>