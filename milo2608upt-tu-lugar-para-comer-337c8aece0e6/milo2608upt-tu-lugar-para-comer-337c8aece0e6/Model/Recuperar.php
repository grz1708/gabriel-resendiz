<?php
namespace proyect\Model;
    class Recuperar extends Conexion
    {
        public $id;
        public $id_usuario;
        public $correo;
        public $codigo;
        public $fecha_expiracion;
        public $status;

        function insert(){
            $pre=mysqli_prepare($this->con,"INSERT INTO recuperar_password(Id_Usuario,Correo,Codigo,Fecha_Expiracion,Status)VALUES(?,?,?,?,?)");
            $pre->bind_param("isiss",$this->id_usuario,$this->correo,$this->codigo,$this->fecha_expiracion,$this->status);
            $pre->execute();
            $pre_=mysqli_prepare($this->con,"SELECT LAST_INSERT_ID() Id");
            $pre_->execute();
            $r=$pre_->get_result();
            $this->id=$r->fetch_assoc()["Id"];
            return true;
        }

        static function findByIdUsuario($idusuario){
            $me=new Conexion();
            $pre=mysqli_prepare($me->con,"SELECT*FROM recuperar_password WHERE Id_Usuario=?");
            $pre->bind_param("i",$idusuario);
            $pre->execute();
            $res =$pre->get_result();
            return $res->fetch_object(Recuperar::class);
        }

        static function find($id){
            $me=new Conexion();
            $pre=mysqli_prepare($me->con,"SELECT*FROM recuperar_password WHERE Id_Usuario=?");
            $pre->bind_param("i",$id);
            $pre->execute();
            $res=$pre->get_result();
            return $res->fetch_assoc();
        }

        static function findcode($code){
            $me=new Conexion();
            $pre=mysqli_prepare($me->con,"SELECT*FROM recuperar_password WHERE Codigo=?");
            $pre->bind_param("i",$code);
            $pre->execute();
            $res=$pre->get_result();
            return $res->fetch_assoc()["Id_Usuario"];
        }

        function update(){
            $me=new Conexion();
            $pre=mysqli_prepare($me->con,"UPDATE recuperar_password SET Status=? WHERE Codigo=?");
            $pre->bind_param("si",$this->status,$this->codigo);
            $pre->execute();
            $res=$pre->get_result();
            return true;
        }

        static function delete($id){
            $me=new Conexion();
            $pre=mysqli_prepare($me->con,"DELETE FROM recuperar_password WHERE Id=?");
            $pre->bind_param("i",$id);
            $pre->execute();
            $res=$pre->get_result();
            return true;
        }

        static function por_correo($id){
            $me=new Conexion();
            $pre=mysqli_prepare($me->con,"SELECT * FROM usuarios WHERE Correo=?");
            $pre->bind_param("s",$id);
            $pre->execute();
            $r=$pre->get_result();
            $e=$r->fetch_assoc()["id"];
            return $e;
        }
    }
    
?>