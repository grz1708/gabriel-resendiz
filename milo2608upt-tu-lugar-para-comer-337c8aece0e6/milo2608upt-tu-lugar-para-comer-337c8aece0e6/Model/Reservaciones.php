<?php
namespace proyect\Model;
    class Reservaciones extends Conexion
    {
        public $id;
        public $id_usuario;
        public $id_restaurante;
        public $nom_cliente;
        public $fecha;
        public $hora;
        function insert(){
            $pre=mysqli_prepare($this->con,"INSERT INTO reservaciones(Id_Usuario,Id_Restaurante,Nom_Cliente,Fecha,Hora)VALUES(?,?,?,?,?)");
            $pre->bind_param("iisss",$this->id_usuario,$this->id_restaurante,$this->nom_cliente,$this->fecha,$this->hora);
            $pre->execute();
            $pre_=mysqli_prepare($this->con,"SELECT LAST_INSERT_ID() Id");
            $pre_->execute();
            $r=$pre_->get_result();
            $this->id=$r->fetch_assoc()["Id"];
            return true;
        }

        static function findBynombrecliente($nom_cliente){
            $me=new Conexion();
            $pre=mysqli_prepare($me->con,"SELECT*FROM reservaciones WHERE Nom_Cliente=?");
            $pre->bind_param("s",$nom_cliente);
            $pre->execute();
            $res =$pre->get_result();
            return $res->fetch_assoc();
        }

        static function find($id){
            $me=new Conexion();
            $pre=mysqli_prepare($me->con,"SELECT*FROM reservaciones WHERE Id=?");
            $pre->bind_param("i",$id);
            $pre->execute();
            $res=$pre->get_result();
             return $res->fetch_array();
        }

        function update(){
            $me=new Conexion();
            $pre=mysqli_prepare($me->con,"UPDATE reservaciones SET Id_Usuario=?, Id_Restaurante=?, Nom_Cliente=?, Fecha=?, Hora=? WHERE Id=?");
            $pre->bind_param("iisssi",$this->id_usuario,$this->id_restaurante,$this->nom_cliente,$this->fecha,$this->hora,$this->id);
            $pre->execute();
            $res=$pre->get_result();
            return true;
        }

        static function delete($id){
            $me=new Conexion();
            $pre=mysqli_prepare($me->con,"DELETE FROM reservaciones WHERE Id=?");
            $pre->bind_param("i",$id);
            $pre->execute();
            $res=$pre->get_result();
            return true;

        }
        static function selectall($idusu){
            $me=new Conexion();
            $pre=mysqli_prepare($me->con,"SELECT * FROM `reservaciones` WHERE Id_Usuario=?");
            $pre->bind_param("i",$idusu);
            $pre->execute();
            $res =$pre->get_result();
            return $res->fetch_all();
        }
    
    }
    
?>