<?php
namespace proyect\Model;
    class Restaurantes extends Conexion
    {
        public $id;
        public $nombre;
        public $direccion;
        public $contacto;
        public $imagen;
        public $horarios;
        public $tipo_comida;

        function insert(){
            $pre=mysqli_prepare($this->con,"INSERT INTO restaurantes(Nombre,Direccion,Contacto,Imagen,Horarios,Tipo_Comida)VALUES(?,?,?,?,?,?)");
            $pre->bind_param("ssssss",$this->nombre,$this->direccion,$this->contacto,$this->imagen,$this->horarios,$this->tipo_comida);
            $pre->execute();
            $pre_=mysqli_prepare($this->con,"SELECT LAST_INSERT_ID() Id");
            $pre_->execute();
            $r=$pre_->get_result();
            $this->id=$r->fetch_assoc()["Id"];
            return true;
        }

        static function find($id){
            $me = new Conexion();
            $pre = mysqli_prepare($me->con, "SELECT * FROM restaurantes where Id=?");
            $pre->bind_param("i", $id);
            $pre->execute();
            $re = $pre->get_result();
            return $re->fetch_assoc();
        }

        static function selectall(){
            $me=new Conexion();
            $pre=mysqli_prepare($me->con,"SELECT*FROM restaurantes");
            $pre->execute();
            $res =$pre->get_result();
            return $res->fetch_all();
        }

        function update(){
            $me=new Conexion();
            $pre=mysqli_prepare($me->con,"UPDATE restaurantes SET Nombre=?, Direccion=?, Contacto=?, Imagen=?, Mesas_Disponibles=?, Horarios=?,Calificacion=?, Tipo_Comida=? WHERE Id=?");
            $pre->bind_param("ssssisisi",$this->nombre,$this->direccion,$this->contacto,$this->imagen,$this->mesas_disponibles,$this->horarios,$this->calificacion,$this->tipo_comida,$this->id);
            $pre->execute();
            $res=$pre->get_result();
            return true;
        }

        static function delete($id){
            $me=new Conexion();
            $pre=mysqli_prepare($me->con,"DELETE FROM restaurantes WHERE Id=?");
            $pre->bind_param("i",$id);
            $pre->execute();
            $res=$pre->get_result();
            return true;
        }
    
    }
    
?>