<?php
namespace proyect\Model;
    class Usuario extends Conexion
    {
        public $id;
        public $nombre;
        public $apellido;
        public $correo;
        public $contraseña;

        function insert()
        {
            $pre = mysqli_prepare($this->con, "INSERT INTO usuarios (nombre, apellido, correo, contraseña) VALUES (?,?,?,?)");
            $pre->bind_param("ssss", $this->nombre, $this->apellido, $this->correo, $this->contraseña);
            $pre->execute();
            $pre_ = mysqli_prepare($this->con, "SELECT LAST_INSERT_ID() id");
            $pre_->execute();
            $r = $pre_->get_result();
            $this->id = $r->fetch_assoc()["id"];
            return true;
        }

        static function findByEmail($correo){
            $me = new Conexion();
            $pre = mysqli_prepare($me->con, "SELECT * FROM usuarios where correo=?");
            $pre->bind_param("s", $correo);
            $pre->execute();
            $re = $pre->get_result();
            return $re->fetch_object(Usuario::class);
        }

        static function find($id){
            $me=new Conexion();
            $pre=mysqli_prepare($me->con,"SELECT*FROM usuarios WHERE id=?");
            $pre->bind_param("i",$id);
            $pre->execute();
            $res=$pre->get_result();
            return $res->fetch_object(Usuario::class);
        }

        static function valid($usu){
            $me=new Conexion();
            $pre=mysqli_prepare($me->con,"SELECT usuario,contrasennia FROM usuarios WHERE usuario=?");
            $pre->bind_param("s",$usu);
            $pre->execute();
            $r=$pre->get_result();
            $dat=$r->fetch_array();
            //$password=$r->fetch_assoc()["Contrasennia"];
            //$arr=[$usu,$password];
            return $dat;
        }

        function update(){
            $me=new Conexion();
            $pre=mysqli_prepare($me->con,"UPDATE `usuarios` SET `nombre` = ?, apellido = ?, correo = ?, contraseña = ? WHERE id = ?");
            $pre->bind_param("ssssi",$this->nombre,$this->apellido,$this->correo,$this->contraseña,$this->id);
            $pre->execute();
            $res=$pre->get_result();
            return true;
        }

        static function delete($id){
            $me=new Conexion();
            $pre=mysqli_prepare($me->con,"DELETE FROM usuarios WHERE id=?");
            $pre->bind_param("i",$id);
            $pre->execute();
            $res=$pre->get_result();
            return true;
        }

        static function show(){
            $me=new Conexion();
            $pre=mysqli_prepare($me->con,"SELECT * FROM usuarios");
            //$pre->bind_param("i",$id);
            $pre->execute();
            $res=$pre->get_result();
            return $res->fetch_assoc();
        }

    }
    
?>